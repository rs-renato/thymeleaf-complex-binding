package br.com.renatorodrigues.thymeleafcomplexbinding.converter;

import br.com.renatorodrigues.thymeleafcomplexbinding.model.Status;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by renatorodrigues on 28/08/17.
 */
public class StatusConverter implements Converter<String, Status> {
	@Override
	public Status convert(String s) {
		return Status.parse(s.charAt(0));
	}
}
