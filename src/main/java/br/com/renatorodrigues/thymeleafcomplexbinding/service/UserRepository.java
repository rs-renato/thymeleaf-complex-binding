package br.com.renatorodrigues.thymeleafcomplexbinding.service;

import br.com.renatorodrigues.thymeleafcomplexbinding.model.Address;
import br.com.renatorodrigues.thymeleafcomplexbinding.model.User;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * Created by renatorodrigues on 15/08/17.
 */
@Repository
public class UserRepository {

	private static Map<Integer, User> users = new HashMap<>();
	
	static {
		
		for (int i = 0; i < 5; i++) {
			
			User user = new User();
			user.setId(i);
			user.setName("user - " + i);
			
			Address address = new Address();
			address.setId(i);
			address.setStreet("Address -  " + i);
			
			user.setAddress(address);
			
			users.put(i,user);
		}
	
	}
	
	public Collection<User> findAll(){
		return users.values();
	}
	
	public void save(User user){
		users.replace(user.getId(), user);
	}
	
	public User findById(Integer userId){
		return users.get(userId);
	}
}
