package br.com.renatorodrigues.thymeleafcomplexbinding.model;

/**
 * Created by renatorodrigues on 15/08/17.
 */
public class User {
	
	private Integer id;
	private String name;
	private Address address;
	private Status status = Status.ATIVO;
	
	public Status getStatus() {
		return status;
	}
	
	public void setStatus(Status status) {
		this.status = status;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Address getAddress() {
		return address;
	}
	
	public void setAddress(Address address) {
		this.address = address;
	}
}
