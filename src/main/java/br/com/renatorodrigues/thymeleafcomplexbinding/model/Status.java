package br.com.renatorodrigues.thymeleafcomplexbinding.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by renatorodrigues on 28/08/17.
 */
public enum Status {
	
	ATIVO('A', "Ativo"),
	INATIVO('I', "Inativo");
	
	private Character valor;
	private String descricao;
	
	private static Map<Character, Status> statusMap;
	
	Status(Character valor, String descricao) {
		this.valor = valor;
		this.descricao = descricao;
		
		init(this);
	}
	
	private void init(Status status) {
		
		if (statusMap == null){
			statusMap = new HashMap<>();
		}
		
		if (!statusMap.containsKey(valor)){
			statusMap.put(valor, this);
		}
	}
	
	public Character getValor() {
		return valor;
	}
	
	public void setValor(Character valor) {
		this.valor = valor;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public static Status parse(Character valor){
		return statusMap.get(valor);
	}
}
