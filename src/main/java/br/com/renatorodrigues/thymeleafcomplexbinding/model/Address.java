package br.com.renatorodrigues.thymeleafcomplexbinding.model;

/**
 * Created by renatorodrigues on 15/08/17.
 */
public class Address {
	
	private Integer id;
	private String street;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getStreet() {
		return street;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
}
