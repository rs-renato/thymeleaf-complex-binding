package br.com.renatorodrigues.thymeleafcomplexbinding;

import br.com.renatorodrigues.thymeleafcomplexbinding.converter.StatusConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication(exclude={org.springframework.boot.autoconfigure.thymeleaf.ThymeleafAutoConfiguration.class} )
public class ThymeleafComplexBindingApplication extends WebMvcConfigurerAdapter {
	
	
	public static void main(String[] args) {
		SpringApplication.run(ThymeleafComplexBindingApplication.class, args);
	}
	
	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addConverter(new StatusConverter());
	}
}
