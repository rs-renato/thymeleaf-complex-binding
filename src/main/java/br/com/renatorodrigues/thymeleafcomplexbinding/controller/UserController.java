package br.com.renatorodrigues.thymeleafcomplexbinding.controller;

import br.com.renatorodrigues.thymeleafcomplexbinding.model.Status;
import br.com.renatorodrigues.thymeleafcomplexbinding.model.User;
import br.com.renatorodrigues.thymeleafcomplexbinding.service.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by renatorodrigues on 15/08/17.
 */
@Controller
@RequestMapping("/users")
public class UserController {
	
	@Autowired
	private UserRepository userRepository;
	
	@GetMapping
	public ModelAndView list(){
		
		ModelAndView mv = new ModelAndView("list");
		
		mv.addObject("users", userRepository.findAll());
		
		return mv;
	}
	
	@GetMapping("/edit/{userId}")
	public ModelAndView edit(@PathVariable Integer userId){
		
		ModelAndView mv = new ModelAndView("edit");
		
		User user = userRepository.findById(userId);
		
		mv.addObject("user", user);
		mv.addObject("status", Status.values());
		
		return mv;
	}
	
	@PostMapping("/save")
	public ModelAndView save(User user){
		
		userRepository.save(user);
		
		return new ModelAndView("redirect:/users");
	}
}
